import * as moment from 'moment';
import * as $ from 'jquery';

let count = 0;

$(function () {
  const queryInfo = {
    url: "https://tv.youtube.com/watch/*",
    audible: true
  };

  var sendYtvTabMessage = (message: any, callback: (result: any) => void) => {
    chrome.tabs.query(queryInfo, tabs => {
      if (tabs[0]) {
        let tab = tabs[0];
        chrome.tabs.sendMessage(tab.id, message, callback);
      } else {
        console.error("No tab found");
      }
    });
  }

  chrome.tabs.query(queryInfo, (result: chrome.tabs.Tab[]) => {
    if (result && result.length > 0) {
      let tab = result[0];
      //let player = tab.
      $('#url').text(result[0].url);

    }
    $('#time').text(moment().format('YYYY-MM-DD HH:mm:ss'));
  });

  chrome.browserAction.setBadgeText({ text: count.toString() });
  $('#countUp').click(() => {
    chrome.browserAction.setBadgeText({ text: (++count).toString() });
  });

  $('#changeBackground').click(() => {
    sendYtvTabMessage({ 'getVideo': true }, result => {
      console.log(result);
    })
  });
});
